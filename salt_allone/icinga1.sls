nagios_central:
  # Install Nagios central server
  pkg.installed:
    - pkgs:
      - icinga-core
      - icinga-common
      - icinga-cgi
      - nagios-images
      - nagios-nrpe-server
      - nagios-nrpe-plugin
      - monitoring-plugins-common
      - monitoring-plugins-basic
      - monitoring-plugins-standard
      # - monitoring-plugins
    - install_recommends: false


nagios_central_ilo:
  # Install Nagios central ilo depedencies
  # No need for libwww-perl libwww-robotrules-perl so install_recommends: false
  cmd.run:
    - name: >
       apt-get --no-act -y --no-install-recommends install
       libmonitoring-plugin-perl libio-socket-ssl-perl libxml-simple-perl
    # - install_recommends: false
#       apt-get -y --no-install-recommends install


nagios_nrpe:
  # Install Nagios nrpe remote client
  pkg.installed:
    - pkgs:
      - nagios-nrpe-server
      # - monitoring-plugins
      # - monitoring-plugins-basic
      - monitoring-plugins-common
      - monitoring-plugins-basic
      - monitoring-plugins-standard
      # - monitoring-plugins
    - install_recommends: false

# nagios    ALL=(ALL) NOPASSWD: /usr/lib/nagios/plugins/, /usr/bin/arrayprobe, /usr/lib64/nagios/plugins/check_*

