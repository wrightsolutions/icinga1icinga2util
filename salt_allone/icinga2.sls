nagios_central:
  # Install Nagios central server
  pkg.installed:
    - pkgs:
      - icinga2
      - icinga2-core
      - monitoring-plugins-common
      - monitoring-plugins-basic
      - monitoring-plugins-standard
      # - monitoring-plugins
      # - icingaweb2
    - install_recommends: false

# nagios    ALL=(ALL) NOPASSWD: /usr/lib/nagios/plugins/, /usr/bin/arrayprobe, /usr/lib64/nagios/plugins/check_*

#Setting up icinga2-bin (2.10.3-2) ...
#enabling default icinga2 features
#Enabling feature checker. Make sure to restart Icinga 2 for these changes to take effect.
#Enabling feature notification. Make sure to restart Icinga 2 for these changes to take effect.
#Enabling feature mainlog. Make sure to restart Icinga 2 for these changes to take effect.
#Created symlink /etc/systemd/system/multi-user.target.wants/icinga2.service  /lib/systemd/system/icinga2.service.
