#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2018 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

import argparse
from datetime import datetime as dt
#from os import getenv as osgetenv
from sys import argv,exit

""" Set values in postgresql.conf based on args.
./sshd79conf.py --listenaddr 'all'
./sshd79conf.py --addrfam 'inet' --clientaliveint 120 --clientlocaleignore
./sshd79conf.py --addrfam inet --clientaliveint 120 --clientlocaleignore --xforward --multiport

Prints to stdout. Might want to redirect (>) to /etc/ssh/sshd_config

Grep the output: python2 ./sshd79conf.py | egrep -i 'isten|port'

/usr/sbin/sshd -T
"""

progfull = argv[0].strip()
from os import path
progbase = path.basename(progfull)
progname = progbase.split('.')[0]
progdir = path.dirname(progbase)

verbosity_global = 1
# Above is set by default to 1. Also see __name__ == '__main__' section later.

SPACER=chr(32)
HASHER=chr(35)
PARENTH_OPEN = chr(40)
PARENTH_CLOSE = chr(41)
# Above are () and next we define {}
BRACES_OPEN = chr(123)
BRACES_CLOSE = chr(125)
ZERO4 = '0.0.0.0'

ADESC="""Arguments for setting vals in default like 7.9 flavoured sshd_config """

SCONF_HEADER4="""#	$OpenBSD: sshd_config,v 1.103 2018/04/09 20:41:22 tj Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.
"""
SCONF_HEADER4_CONF_RC="""#	$OpenBSD: sshd_config,v 1.103 2018/04/09 20:41:22 tj Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.
## Non-zero return from generation encountered! (conf_rc={0})
"""
SCONF_TEMPLATE="""#	$OpenBSD: sshd_config,v 1.103 2018/04/09 20:41:22 tj Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

# This sshd was compiled with PATH=/usr/bin:/bin:/usr/sbin:/sbin

# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.

"""

SCONF_ADDRPORT="""
#Port 22
#AddressFamily any
AddressFamily {addrfam}
#ListenAddress 0.0.0.0
#ListenAddress ::
{hashlisten}ListenAddress {listenaddr}
"""

SCONF_LOGGINGPLUS="""
#HostKey /etc/ssh/ssh_host_rsa_key
#HostKey /etc/ssh/ssh_host_ecdsa_key
#HostKey /etc/ssh/ssh_host_ed25519_key

# Ciphers and keying
#RekeyLimit default none

# Logging
#SyslogFacility AUTH
#LogLevel INFO

"""

SCONF_GLOBAL_PERMIT_AND_MAX="""# Authentication:
#LoginGraceTime 2m
#PermitRootLogin prohibit-password
PermitRootLogin prohibit-password
{hashpermitrootlogin}PermitRootLogin {permitrootlogin}
#StrictModes yes
#MaxAuthTries 6
#MaxSessions 10
"""

SCONF_AUTH="""
#PubkeyAuthentication yes

# Expect .ssh/authorized_keys2 to be disregarded by default in future.
#AuthorizedKeysFile	.ssh/authorized_keys .ssh/authorized_keys2

#AuthorizedPrincipalsFile none

#AuthorizedKeysCommand none
#AuthorizedKeysCommandUser nobody

# For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
#HostbasedAuthentication no
# Change to yes if you don't trust ~/.ssh/known_hosts for
# HostbasedAuthentication
#IgnoreUserKnownHosts no
# Don't read the user's ~/.rhosts and ~/.shosts files
#IgnoreRhosts yes

# To disable tunneled clear text passwords, change to no here!
#PasswordAuthentication no
{hashpasswordauth}PasswordAuthentication {passwordauth}
#PermitEmptyPasswords no

# Change to yes to enable challenge-response passwords (beware issues with
# some PAM modules and threads)
ChallengeResponseAuthentication no

# Kerberos options
#KerberosAuthentication no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes
#KerberosGetAFSToken no

# GSSAPI options
#GSSAPIAuthentication no
#GSSAPICleanupCredentials yes
#GSSAPIStrictAcceptorCheck yes
#GSSAPIKeyExchange no

# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. If this is enabled, PAM authentication will
# be allowed through the ChallengeResponseAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via ChallengeResponseAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and ChallengeResponseAuthentication to 'no'.
UsePAM yes
"""
SCONF_DNSCHROOT="""
PrintMotd no
#PrintLastLog yes
#TCPKeepAlive yes
#PermitUserEnvironment no
#Compression delayed
#ClientAliveInterval 0
{hashclientaliveint}ClientAliveInterval {clientaliveint}
#ClientAliveCountMax 3
## Not in sshd_config: ServerAliveCountMax 3
#UseDNS no
#PidFile /var/run/sshd.pid
#MaxStartups 10:30:100
#PermitTunnel no
#ChrootDirectory none
#VersionAddendum none

# no default banner path
#Banner none

# Allow client to pass locale environment variables
#AcceptEnv EDITOR LANG LC_ALL
AcceptEnv LANGUAGE
{hashacceptenv}AcceptEnv LANG LC_*

"""

SCONF_SECTION_FORWARD_AND_X11="""
#AllowAgentForwarding yes
#AllowTcpForwarding yes
#GatewayPorts no
X11Forwarding no
{hashxforward}X11Forwarding {xforward}
#X11DisplayOffset 10
#X11UseLocalhost yes
#PermitTTY yes
"""

SCONF_SECTION_OVER="""
# override default of no subsystems
Subsystem	sftp	/usr/lib/openssh/sftp-server

# Example of overriding settings on a per-user basis
#Match User anoncvs
#	X11Forwarding no
#	AllowTcpForwarding no
#	PermitTTY no
#	ForceCommand cvs server
"""

def false_unless_true(bool_str):
	""" Process an arg into lowercase true or false 
	based on appropriate interpretation of the string.
	Although argument is labelled 'bool_str' we accept and
	deal with other cases typical of a non validated input
	In argparse might want type=false_unless_true (untested)
	"""
	bool_return = False
        # Do keep False as the default or trace conditions carefully before changing
	try:
		blower = bool_str.strip().lower()
	except:
		# Not a string
		if bool_str is True:
			bool_return = True
		else:
			# False or some other non-string value
			pass
			#print(bool_str,bool_str)
	try:
		blower_int = int(blower)
		if 1 == blower_int:
			bool_return = True
	except:
		if bool_return is False:
			if bool_str is False:
				pass
			else:
				try:
					if blower in ('true', 't', 'yes'):
						bool_return = True
				except:
					EMSG_PREFIX = 'Unexpected value supplied: '
					raise Exception(EMSG_PREFIX+bool_str)

	return bool_return


def false_unless_true_lowercase(bool_str):
	""" Wrapper around false_unless_true where we REALLY want
	a response that is a stringified lowercase true or false
	"""
	bool_tf = false_unless_true(bool_str)
	bool_lower = 'true'
	if bool_tf is False:
		bool_lower = 'false'
	return bool_lower


def lines_hashed(lines,stripper=None):
	""" Count of lines beginning hash in the list
	of lines supplied
	"""
	hasher_count = 0
	for line in lines:
		if 'left' == stripper:
			if line.lstrip().startswith(HASHER):
				hasher_count+=1
		else:
			if line.startswith(HASHER):
				hasher_count+=1
	return hasher_count


def int_and_suffixed_correctly(numeric_string,default='2MB'):
	""" Make M into MB and G into GB and so numeric like
	string values related to memory have expected suffixes
	Second parameter (default) replaces the given numeric string
	when numeric_string supplied is None or blank or other
	easily detectable problem
	"""
	if numeric_string is None:
		return default
	elif '' == numeric_string:
		return default
	elif len(numeric_string.strip()) < 2:
		# Two is minimum length if suffix is M or G
		return default
	else:
		pass

	nstripped = numeric_string.strip()

	if nstripped.endswith('MB') or nstripped.endswith('GB'):
		return nstripped

	try:
		nlower = numeric_string.lower()
	except:
		return default

	# Next we make K into KB, M into MB, G into GB, ...
	nsuffixed = nstripped
	if nlower.endswith('k'):
		nsuffixed = "{0}KB".format(nstripped[:-1])
	elif nlower.endswith('m'):
		nsuffixed = "{0}MB".format(nstripped[:-1])
	elif nlower.endswith('g'):
		nsuffixed = "{0}GB".format(nstripped[:-1])
	else:
		pass

	return nsuffixed


if __name__ == '__main__':

	args_all = argv[1:]
	try:
		args_all_joined = SPACER.join(args_all)
	except:
		args_all_joined = args_all

	par = argparse.ArgumentParser(description=ADESC)
	# Avoid type=bool or similar as that kind of processing is after args assigned here.
	par.add_argument('--inet', default=None,
				action='store',
				dest='inet',
				help='inet keyword will set AddressFamily=inet')
	par.add_argument('--addrfam', default='any',
				action='store',
				dest='addrfam',
				help='Desired AddressFamily (e.g.) any')
	par.add_argument('--listenaddr', default=None,
				action='store',
				dest='listenaddr',
				help='Desired ListenAddress (e.g.) 0.0.0.0')
	par.add_argument('--clientaliveint', default=None,
				action='store', type=int,
				dest='clientaliveint',
				help='supplying 120 would see ClientAliveInterval 120')
	par.add_argument('--clientlocaleignore', default=None,
				action='store_true',
				dest='clientlocaleignore',
				help='Indicates you want to force ignore of the client LOCALE / LANG_LC')
	par.add_argument('--permitrootlogin', default='forced-commands-only',
				action='store',
				dest='permitrootlogin',
				help='Desired policy for login to root (e.g.) prohibit-password or yes')
	par.add_argument('--passwordauth', default=None,
				action='store_true',
				dest='passwordauth',
				help='--passwordauth would make PasswordAuthentication yes')
	par.add_argument('--xforward', default=None,
				action='store_true',
				dest='xforward',
				help='--xforward would make X11Forwarding yes')
	par.add_argument('--multiport', default=None,
				action='store_true',
				dest='multiport',
				help='--multiport would make ssh listen on port 22 and others')
	args = par.parse_args()

	#installonshutdown = false_unless_true_lowercase(args.installonshutdown)
	#tz_stripped = args.timezone.strip()
	#maintworkmem = int_and_suffixed_correctly(args.maintworkmem.strip(),'32MB')

	addrfam = 'any'
	if args.addrfam is None:
		pass
	elif args.addrfam == ZERO4:
		pass
	else:
		addrfam = args.addrfam
	if args.inet:
		addrfam = 'inet'
	hashlisten = HASHER
	listenaddr = ZERO4
	if args.listenaddr is None:
		pass
	elif args.listenaddr == ZERO4:
		pass
	elif args.listenaddr in ['any','all']:
		# Although 'any' and 'all' are not legal values for ListenAddress, we will interpret them as ZERO4
		pass
	else:
		listenaddr = args.listenaddr
		hashlisten = ''

	clientlocaleignore = args.clientlocaleignore

	dict_args = {'addrfam': addrfam,
		'hashlisten': hashlisten,
		'listenaddr': listenaddr
	}

	sconf_addrport = SCONF_ADDRPORT.format(**dict_args)
	if args.multiport is True:
		# 22,2222,443,62222,15020
		sconf = SCONF_HEADER4
		sconf += 'Port 22\n'
		sconf += 'Port 2222\n'
		sconf += 'Port 443\n'
		sconf += 'Port 62222\n'
		sconf += 'Port 15020'
		sconf += sconf_addrport
	else:
		sconf = SCONF_HEADER4 + sconf_addrport

	if lines_hashed(sconf) < 2:
		conf_rc = 201
	elif BRACES_OPEN in sconf:
		# Found a '{' which is opener for format() replacement!
		conf_rc = 211
		sconf = SCONF_HEADER4_CONF_RC.format(conf_rc)
	elif BRACES_CLOSE in sconf:
		# Found a '}' which is closer for format() replacement!
		conf_rc = 212
		sconf = SCONF_HEADER4_CONF_RC.format(conf_rc)
	else:
		conf_rc = 200

	idx = 0
	for idx,line in enumerate(sconf.splitlines()):
		print(line)
	if idx < 3:
		exit(conf_rc)

	linecount = idx

	idx = 0
	for idx,sline in enumerate(SCONF_LOGGINGPLUS.splitlines()):
		print(sline)

	linecount += idx

	hashpermitrootlogin = HASHER
	if args.permitrootlogin is None:
		pass
	else:
		permitrootlogin = args.permitrootlogin.strip()
		if permitrootlogin == 'forced-commands-only':
			pass
		else:
			hashpermitrootlogin = ''
	dict_args = {'hashpermitrootlogin': hashpermitrootlogin,
		'permitrootlogin': permitrootlogin,
	}

	sconf_global_permit_and_max = SCONF_GLOBAL_PERMIT_AND_MAX.format(**dict_args)

	idx = 0
	for idx,sline in enumerate(sconf_global_permit_and_max.splitlines()):
		print(sline)

	linecount += idx

	hashpasswordauth = HASHER
	if args.passwordauth is None:
		passwordauth = 'no'
	else:
		passwordauth = args.passwordauth.strip()
		hashpasswordauth = ''

	dict_args = {'hashpasswordauth': hashpasswordauth,
		'passwordauth': passwordauth,
	}

	sconf_auth = SCONF_AUTH.format(**dict_args)

	idx = 0
	for idx,sline in enumerate(sconf_auth.splitlines()):
		print(sline)

	linecount += idx

	hashxforward = HASHER
	xforward = 'no'
	if args.xforward is None:
		pass
	else:
		xforward = 'yes'
		hashxforward = ''
	dict_args = {'hashxforward': hashxforward,
		'xforward': xforward,
	}

	sconf_section_forward_and_x11 = SCONF_SECTION_FORWARD_AND_X11.format(**dict_args)

	idx = 0
	for idx,sline in enumerate(sconf_section_forward_and_x11.splitlines()):
		print(sline)

	linecount += idx

	hashacceptenv = ''
	if args.clientlocaleignore is not None:
		hashacceptenv = HASHER
	hashclientaliveint = HASHER
	clientaliveint = 0
	if args.clientaliveint > 0:
		clientaliveint = int(args.clientaliveint)
		hashclientaliveint = ''
	dict_args = {'hashclientaliveint': hashclientaliveint,
	'clientaliveint': clientaliveint,
	'hashacceptenv': hashacceptenv,
	}
	sconf_dnschroot = SCONF_DNSCHROOT.format(**dict_args)

	idx = 0
	for idx,sline in enumerate(sconf_dnschroot.splitlines()):
		print(sline)

	linecount += idx

	idx = 0
	for idx,sline in enumerate(SCONF_SECTION_OVER.splitlines()):
		print(sline)

	linecount += idx

	conf_rc = 0	# Excellent (and expected) result so 0 return set
	print("{0}{0} args: {1}".format(HASHER,args_all))
	dtiso = dt.now().isoformat()
	tail1 = "# Generated {0} lines at {1} ".format(linecount,dtiso)
	tail2 = "based on user args of: {0}".format(args_all_joined)
	print("#{0}{1}".format(tail1,tail2))

	exit(conf_rc)

