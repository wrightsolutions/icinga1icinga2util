#!/bin/sh
# Until you have a valid ssl cert then your browser message might read one of following
# Firefox: ssl_error_rx_record_too_long
# Chromium: ERR_SSL_PROTOCOL_ERROR
# A misconfigured proxy can also result in similar errors
# Step 1: Check the provided ssl or generate a self-signed using command next...
printf "When prompted for Common Name (e.g. server FQDN or YOUR name) ...\n"
printf "...enter FQDN (server hostname fully qualified) or for test setups the IP address instead.\n"
openssl req -x509 -nodes -days 1000 -newkey rsa:4096 -keyout /etc/ssl/private/apache24selfsigned.key -out /etc/ssl/certs/apache24selfsigned.crt
